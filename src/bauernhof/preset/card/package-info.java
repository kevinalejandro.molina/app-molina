/**
 * Everything related to cards.
 *
 * <p style="color:#31708f;background-color:#d9edf7;border-color:#bce8f1;padding:1em;margin-bottom:1.2em;border:0.1em solid transparent;border-radius:0.25em;"> <!-- INFO -->
 * You can draw a {@link Card} using {@link GCard}.
 * </p>
 *
 *
 * <div style="color:#8a6d3b;background-color:#fcf8e3;border-color:#faebcc;padding:1em;margin-bottom:1.2em;border:0.1em solid transparent;border-radius:0.25em;"> <!-- WARNING -->
 * Don't forget to also look at the other packages:
 * <ul>
 * <li>{@link bauernhof.preset}</li>
 * <li>{@link bauernhof.preset.networking}</li>
 * </ul>
 * </div>
 *
 */
package bauernhof.preset.card;
