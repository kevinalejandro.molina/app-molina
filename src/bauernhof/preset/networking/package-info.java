/**
 * Everything related to networking.
 *
 * <p style="color:#31708f;background-color:#d9edf7;border-color:#bce8f1;padding:1em;margin-bottom:1.2em;border:0.1em solid transparent;border-radius:0.25em;"> <!-- INFO -->
 * The server uses {@link S2CConnection} and {@link RemotePlayer}.<br>
 * The client uses {@link C2SConnection}.
 * </p>
 *
 *
 * <div style="color:#8a6d3b;background-color:#fcf8e3;border-color:#faebcc;padding:1em;margin-bottom:1.2em;border:0.1em solid transparent;border-radius:0.25em;"> <!-- WARNING -->
 * Don't forget to also look at the other packages:
 * <ul>
 * <li>{@link bauernhof.preset}</li>
 * <li>{@link bauernhof.preset.card}</li>
 * </ul>
 * </div>
 *
 */
package bauernhof.preset.networking;
