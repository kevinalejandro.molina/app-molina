package bauernhof.preset;


/**
 * Optional features you may want to implement.
 */
public enum OptionalFeature {
	/**
	 * Schreiben Sie einen einfachen regelbasierten Computerspieler.
	 */
	SIMPLE_AI,
	/**
	 * Entwickeln Sie einen weiteren, intelligenteren Computerspieler.
	 */
	ADVANCED_AI,
	/**
	 * Implementieren Sie einen Tournament-Modus, bei dem mehrere (Computer-)Spieler mehrere (viele) Spiele hintereinander Spielen können. Erstellen Sie eine Statistik der Spielergebnisse.
	 */
	TOURNAMENTS,
	/**
	 * Laden und Speichern von Spielständen. Hierfür darf den {@link Player Spielern} im Konstruktor ihre aktuelle Hand übergeben werden.
	 */
	SAVEGAMES,
	/**
	 * Animieren Sie Elemente und Interaktionen mit der grafischen Oberfläche.
	 */
	ANIMATIONS,
	/**
	 * Verschönern Sie die Karten.
	 */
	BETTERGRAPHICS,
	/**
	 * Entwickeln Sie einen Launcher mit dem sich die Einstellungen editieren lassen.
	 */
	LAUNCHER,
	/**
	 * Ermöglichen Sie das Tätigen von Screenshots der aktuellen Grafik.
	 */
	SCREENSHOTS,
	/**
	 * Fügen Sie Soundeffekte zu Interaktionen hinzu.
	 */
	SOUNDEFFECTS,
	/**
	 * Entwickeln Sie einen grafischen Spielkonfigurationseditor oder Karteneditor.
	 */
	EDITOR
}

