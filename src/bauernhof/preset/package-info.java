/**
 * General classes, enums and interfaces of the preset.
 *
 * <div style="color:#31708f;background-color:#d9edf7;border-color:#bce8f1;padding:1em;margin-bottom:1.2em;border:0.1em solid transparent;border-radius:0.25em;"> <!-- INFO -->
 * You should probably start by looking at the following classes in here:
 * <ul>
 * <li>{@link Settings}, {@link ArgumentParser} and {@link OptionalFeature}</li>
 * <li>{@link Logger}</li>
 * <li>{@link GameConfiguration} and {@link GameConfigurationParser}</li>
 * <li>{@link Player}, {@link PlayerGUIAccess} and {@link ImmutableList}</li>
 * </ul>
 * </div>
 *
 *
 * <div style="color:#8a6d3b;background-color:#fcf8e3;border-color:#faebcc;padding:1em;margin-bottom:1.2em;border:0.1em solid transparent;border-radius:0.25em;"> <!-- WARNING -->
 * Don't forget to also look at the other packages:
 * <ul>
 * <li>{@link bauernhof.preset.card}</li>
 * <li>{@link bauernhof.preset.networking}</li>
 * </ul>
 * </div>
 *
 */
package bauernhof.preset;
