package bauernhof.preset;

/**
 * The different possible loglevels for the {@link Logger}.
 */
public enum LogLevel {
	/**
	 * Dangerous errors.
	 */
	ERRORS,
	/**
	 * Alarming warnings.
	 */
	WARNINGS,
	/**
	 * Useful information.
	 */
	INFO,
	/**
	 * Lots and lots of debugging output.
	 */
	DEBUG
}
