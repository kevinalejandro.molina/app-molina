package app.projekt;

import java.awt.Color; 
import sag.*; 
import sag.elements.*; 
import sag.elements.shapes.*;
import sag.events.*;

import bauernhof.preset.card.GCard;

public class GKarte extends GCard {
	private Karte karte;
	private Spieler spieler;

	private MouseEventListener getMouseEventListener(GKarte gkarte) {
        return new MouseEventListener() {
            @Override
            public void mouseClicked(MouseButtonEvent event, GElement self) {
		    	Spielbrett sb = Spielbrett.getInstance();
				if (spieler.getReadyToDiscard()) {
					sb.performHumanMove(gkarte.karte.getIndex());
				} else if (sb.getDepoCard() != null) {
					int index = sb.getDepoCard().getKarte().getIndex();
					Karte newKarte = sb.getDiscardPile().removeCardAt(index);
					sb.performHumanMove(gkarte.karte.getIndex(), newKarte);
				} else  {
					System.out.println("BAD: chose a card to Draw");
				}
            }

            @Override
            public void mouseWheelMoved(MouseWheelEvent event, GElement self) {
                // Implementación básica
            }

            @Override
            public void mouseMoved(MouseMotionEvent event, GElement self) {
                // Implementación básica
            }

            @Override
            public void mouseExited(MouseMotionEvent event, GElement self) {
				self.setStroke(Color.BLACK);
            }

            @Override
            public void mouseEntered(MouseMotionEvent event, GElement self) {
				self.setStroke(Color.GREEN);
            }

            @Override
            public void mouseReleased(MouseButtonEvent event, GElement self) {
                // Implementación básica
            }

            @Override
            public void mousePressed(MouseButtonEvent event, GElement self) {
                // Implementación básica
            }
        };
    }

    public GKarte(Karte karte) {
        super(karte);
		this.karte = karte;
		this.spieler = (Spieler) Spielbrett.getInstance().getActualPlayer();
		this.setListener();
    }

	private void setListener() {
		this.setMouseEventListener(getMouseEventListener(this));
	}
	
	public Karte getKarte() {
		return this.karte;
	}

}

