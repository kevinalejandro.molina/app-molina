package app.projekt;

import app.projekt.UI.*;
import java.awt.Color; 
import sag.*; 
import sag.elements.*; 
import sag.elements.shapes.*;
import sag.events.*;
import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

import bauernhof.preset.card.Card;


public class GUI {
	private SAGFrame frame;
	private SAGPanel panel;
	private GGroup[] layers;
	private int cards;
	private int depo;
	private Counter cardsC;
	private Counter depoC;
	private CountDownLatch startSignal;
	private DeckButton deckB;
	private PlayerGUI pgui;

	private static GUI instance = null;

	private GUI(String name, int cards, int depo, CountDownLatch startSignal) {
		this.frame = new SAGFrame(name,30,1280,720);
		this.panel = new SAGPanel(1600,900);            
		this.layers = new GGroup[9];
		this.cards = cards;
		this.depo = depo;

		this.startSignal = startSignal;

		this.initLayers();
    }

	public static GUI getInstance(String name, int cards, int depo, CountDownLatch startSignal) {
		if(instance == null) {
			instance = new GUI(name, cards, depo, startSignal);
		}
		return instance;
	}

	public static GUI getInstance() {
		if(instance == null) {
			throw new IllegalStateException("GUI instance not initialized.");
		}
		return instance;
	}

	public void initLayers() {
		this.layers[0] = panel.addLayer(LayerPosition.TOP_LEFT);
		this.layers[1] = panel.addLayer(LayerPosition.TOP_CENTER);
		this.layers[2] = panel.addLayer(LayerPosition.TOP_RIGHT);
		this.layers[3] = panel.addLayer(LayerPosition.CENTER_LEFT);
		this.layers[4] = panel.addLayer(LayerPosition.CENTER_CENTER);
		this.layers[5] = panel.addLayer(LayerPosition.CENTER_RIGHT);
		this.layers[6] = panel.addLayer(LayerPosition.BOTTOM_LEFT);
		this.layers[7] = panel.addLayer(LayerPosition.BOTTOM_CENTER);
		this.layers[8] = panel.addLayer(LayerPosition.BOTTOM_RIGHT);
	}

	public GGroup[] getLayers() {
		return this.layers;
	}

	private void setCards(int layer, int number){
		Counter counter = new Counter(120,50,layers[layer],number,0);    
		this.cardsC = counter;
	}

	private void setDepo(int layer, int number){
		Counter counter = new Counter(-180,50,layers[layer],number,10);
		this.depoC = counter;
	}

	private void setStart(int layer, String text){
		Button button = new Button(0f,-200f,layers[layer],text, startSignal);
	}

	public void setPlayer(){
		PlayerGUI playerGui = new PlayerGUI(-200f,100f,layers[2]);
		this.pgui = playerGui;
	}

    public void updatePlayerGUI(){
		this.pgui.updateGUI();
	}

	public void setButtons(){
		Button all = new Button(100f,80,layers[0], "Überblick");
		//Button hands = new Button(100f,80f,layers[0], "See Hands");
	}

	public void setDeck(GGroup layer){
		DeckButton deck = new DeckButton(layer);
		this.deckB = deck;
	}

	public void initPage() {
		frame.setSAGPanel(panel);
		frame.setVisible(true);

		GCircle mycircle = new GCircle(50);
		mycircle.setFill(Color.GREEN);
		setCards(3,cards);
		setDepo(5,depo);
		setStart(7, "Start");
	}

	public int getCards() {
		return this.cardsC.getValue();
	}

	public int getDepo() {
		return this.depoC.getValue();
	}

	public SAGPanel getPanel() {
		return this.panel;
	}

	public SAGFrame getFrame() {
		return this.frame;
	}

	public void clean() {
		this.frame.getContentPane().removeAll();
		this.frame.revalidate();
		this.frame.repaint();
		this.panel = new SAGPanel(1600,900);            
		this.initLayers();
		this.frame.setSAGPanel(this.panel);
		this.frame.setVisible(true);
	}
	
	public void cleanLayer(int index) {
		List<Drawable> children = new ArrayList<>();
		this.layers[index].iterator().forEachRemaining(children::add);

		for (Drawable child : children) {
			this.layers[index].removeChild(child);
		}
	}
    
	public void sketchCards(List<? extends Card> hand, int y, int layer) {
		int shift = hand.size();
		this.cleanLayer(layer);
		if (hand.size() <= 8) {
			shift -= 1;
			shift *= -100;
		}
        for (Card card : hand) {
		    GKarte gkarte = new GKarte((Karte) card);
			gkarte.setPosition(shift,-y);
			shift += 200;
		    this.layers[layer].addChild(gkarte);
        }
	}

	public void sketchCards(List<? extends Card> dp, int y, int layer, int shift) {
		shift = dp.size();
		int column = 0;
    	int row = 0;

		int cardsInRow = Math.min(dp.size(), 7);
		int startX = -(cardsInRow - 1) * 200 / 2 + 100;

    
		this.cleanLayer(layer);

        for (Card card : dp) {
			//System.out.println(card.getName());
		    GDiscardPileCard gkarte = new GDiscardPileCard((Karte) card);
			int x = startX + column * 200;
			int newY = y - row * 300;
			gkarte.setPosition(x,newY);
			column++;
			if (column >= cardsInRow) {
				column = 0;
				row++;
			}
			
		    this.layers[layer].addChild(gkarte);
        }
	}

	public void sketchDiscardPile(List<? extends Card> discardPile, int y, int layer) {
		sketchCards(discardPile, 0, layer,0);
		deckB.redraw(this.layers[layer], this.calculateShift(discardPile.size()));
	}

	private float calculateShift(int x) {
		if (x > 7) {
			return  -700;
		}
		return (x+1) * -100 + 100;
	}
		
}
