package app.projekt;

import java.awt.Color; 
import sag.*; 
import sag.elements.*; 
import sag.elements.shapes.*;
import sag.events.*;

import bauernhof.preset.Move;

import javax.swing.Timer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class Spielbrett {
	private Deck deck;
    private Deck discardPile;
    private List<Spieler> players;
    private GUI gui;
	private SAGFrame frame;
	private SAGPanel panel;
	private GGroup[] layers;
    private static Spielbrett instance; // For Singleton pattern
	private int actualTurn;
	private GDiscardPileCard depoCard;
	int m;

    private Spielbrett(List<Spieler> players, Deck deck, int m) {
		this.gui = GUI.getInstance();
		this.frame = gui.getFrame();
        this.players = players;
        this.deck = deck;
        this.discardPile = new Deck();
		this.panel = panel;
		this.m = m;

		this.gui.initLayers();

        this.actualTurn = 0;

    }

	public static Spielbrett getInstance(List<Spieler> spielern, Deck deck, int m) {
        if (instance == null) {
            instance = new Spielbrett(spielern, deck, m);
        }
        return instance;
    }

	public static Spielbrett getInstance() {
        if (instance == null) {
            throw new IllegalStateException("Spielbrett instance has not been initialized yet");
        }
        return instance;
    }

    public void nextPlayer() {
		if (isGameOver()) {
			System.out.println("Game is finish");
			System.exit(0);
		}
		Timer timer = new Timer(1000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				actualTurn = (actualTurn + 1) % players.size();
				preparePlayer(actualTurn);
			}
		});
		timer.setRepeats(false); 
		timer.start();
    }

	public boolean isGameOver(){
		if (this.discardPile.size() >= m) {
			return true;
		}
		return false;
	}

	public Deck getDeck() {
		return this.deck;
	}

	public Deck getDiscardPile() {
		return this.discardPile;
	}
		
	public void printDiscardPile() {
		List<Karte> cards = discardPile.getCards(); 

		if (cards.size() > 0) {
        	System.out.println("Discard pile:");
			for (int i = 0; i < cards.size(); i++) {
    			Karte card = cards.get(i);
        		System.out.print(i+1 + " - ");
				System.out.println(card.getName()); // Asume que Card tiene un método toString() adecuado.
			}
		} else {
        	System.out.println("Discard pile empty");
		}
	}

	public void sketchDiscardPile() {
		GUI gui = GUI.getInstance();
		gui.sketchDiscardPile(this.getDiscardPile().getCards(), 100, 4);
	}

	public void startGame(int h) {
        // Shuffle the deck.
        deck.shuffle();

        // Clear the discard pile.
        discardPile.clear();

		
        // Number the players and give them cards.
        for (int i = 0; i < players.size(); i++) {
            Spieler player = players.get(i);
            player.setId(i);  // Players are numbered from 0 to k-1.
            
            // Each player takes h cards from the deck.
            for (int j = 0; j < h; j++) {
                if (!deck.isEmpty()) {
                    Karte card = deck.drawCard();
                    player.addCard(card, j);
                }
            }
        }
		// Put Controls
        this.gui.setPlayer();
        this.gui.setButtons();

		this.preparePlayer(0);
		this.gui.setDeck(this.gui.getLayers()[4]);

	}

	private void preparePlayer(int index) {
		players.get(index).sketchCards();
		gui.updatePlayerGUI();
		if(this.getActualPlayer() instanceof Bot) {
			Bot bot = (Bot) this.getActualPlayer();
			bot.playTurn(this.getDiscardPile().size());
		}
	}

	public void performHumanMove(int cardIndex) {
		try {
			Spieler player = getActualPlayer();
			if (player instanceof Human) {
				Karte chosenCard = (Karte) player.getCardAt(cardIndex);

				player.chosedCard(cardIndex);
				instance.getDiscardPile().addCard((Karte) player.getCardAt(cardIndex));
				Move move = player.request();

				instance.printDiscardPile();
				instance.sketchDiscardPile();

				this.nextPlayer();
			} else if (player instanceof Bot) {
				System.out.println("");
				Karte chosenCard = (Karte) player.getCardAt(cardIndex);

				player.chosedCard(cardIndex);
				instance.getDiscardPile().addCard((Karte) player.getCardAt(cardIndex));
				Move move = player.request();

				instance.printDiscardPile();
				instance.sketchDiscardPile();

				this.nextPlayer();
				//System.exit(0);
			} else {
				// IF ERROR
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

    }

    public void performHumanMove(int cardIndex, Karte newCard) {
		Spieler player = getActualPlayer();
		player.setNewCard(newCard);
		player.addCard(newCard, cardIndex);
		performHumanMove(cardIndex);
    }

	

	public Spieler getActualPlayer() {
        return players.get(actualTurn);
    }

    public GDiscardPileCard getDepoCard() {
        return this.depoCard;
    }

	public void selectDepoCard(GDiscardPileCard karte) {
		if (this.depoCard != null) {
            this.depoCard.deselect();
        }

        this.depoCard = karte;
        karte.select();
	}

    public void deselectDepoCard() {
        if (this.depoCard != null) {
            this.depoCard.deselect();
			this.depoCard = null;
        }
    }
}

