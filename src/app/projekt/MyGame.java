package app.projekt;

import java.awt.Color; 
import sag.*; 
import sag.elements.*; 
import sag.elements.shapes.*;
import java.util.List;
import java.util.ArrayList;
import java.io.File;
import java.io.IOException;  
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;


import bauernhof.preset.card.GCard;
import bauernhof.preset.OptionalFeature;
import bauernhof.preset.GameConfigurationException;
import bauernhof.preset.Settings;
import bauernhof.preset.PlayerType;
import bauernhof.preset.Move;

	
public class MyGame {
	public static void main(String[] args) {
		CountDownLatch startSignal = new CountDownLatch(1);
	    Scanner scanner = new Scanner(System.in);


		List<String> autores = new ArrayList<String>();
		autores.add("Molina Kevin");
		autores.add("Joussef");
		List<OptionalFeature> features = new ArrayList<>();
		//features.add(myFeature);  // Asume que myFeature es una instancia de OptionalFeature
		ArgumentParser parser = new ArgumentParser(args, "Fantastischer Hof", "1.0", autores, features);



		// Config Parser
		Config gameConfig = null;

		try {
			// Crea una instancia de ConfigParser
			ConfigParser configParser = new ConfigParser();
			
			// Crea una instancia de File para el archivo XML que deseas parsear
			File xmlFile = new File("src/app/projekt/ressources/config.xml");
			
			// Utiliza ConfigParser para parsear el archivo XML
			gameConfig = (Config) configParser.parse(xmlFile);
			
			// Ahora puedes utilizar gameConfig como desees...
		} catch (GameConfigurationException | IOException e) {
			// Maneja cualquier excepción que pueda ocurrir durante el parseo
			e.printStackTrace();
		}

		List<Spieler> spielern = new ArrayList<Spieler>();
		Deck deck = null;
		// Verificar si gameConfig se inicializó correctamente
		if (gameConfig != null) {
			// Create Deck
			deck = new Deck(gameConfig.getKarten());

			// Create players based on --playerTypes flag
			spielern.add(new Human("Hum"));
			spielern.add(new Bot("Bot1"));
			spielern.add(new Bot("Bot2"));
			spielern.add(new Bot("Bot3"));


		} else {
			System.out.println("ERROR: No gameConfig");
			System.exit(0);
		} 


		// GUI
		GUI gui = GUI.getInstance("Fantastischer Hof", gameConfig.getNumCardsPerPlayerHand(), gameConfig.getNumDepositionAreaSlots(), startSignal);
		gui.initPage();

		// Wait until start button
		try {
			startSignal.await();  
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		gui.clean();
		gui.initLayers();
		Spielbrett sb = Spielbrett.getInstance(spielern, deck, gui.getDepo());
		sb.startGame(gui.getCards());

		while(!sb.isGameOver()) {
			for (Spieler spieler : spielern) {
			}
		}

		System.out.println("FINISH HIM");


	}
}
