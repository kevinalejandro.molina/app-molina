package app.projekt;

import java.awt.Color; 
import sag.*; 
import sag.elements.*; 
import sag.elements.shapes.*;
import sag.events.*;

import bauernhof.preset.Player;
import bauernhof.preset.Move;
import bauernhof.preset.card.Card;
import bauernhof.preset.GameConfiguration;
import java.util.List;
import java.util.ArrayList;

import com.google.common.collect.ImmutableList;

public class Spieler implements Player {
    private String name;
    private GameConfiguration gameConfiguration;
    private List<Card> hand = new ArrayList<>();
    private int id;
    private int score;
	private int chosedIndex;
	protected Card newCard;
    protected boolean readyToDiscard;

    public Spieler(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
	public void init(GameConfiguration gameConfiguration, ImmutableList<Card> initialDrawPile, int numplayers, int playerid) throws Exception {
        this.gameConfiguration = gameConfiguration;
        this.hand.addAll(initialDrawPile);
        this.id = playerid;
    }

	@Override
	public Move request() throws Exception {
		// Asegúrate de tener al menos dos cartas en tu mano.
		if (hand.size() < 2) {
			throw new Exception("No hay suficientes cartas en la mano para hacer un movimiento.");
		}

		// Usa las dos primeras cartas de tu mano para hacer un movimiento.
		Karte card1 = (Karte) hand.get(0);
		Karte card2 = (Karte) hand.get(1);

		// Crea y devuelve una nueva instancia de Move utilizando estas dos cartas.
        Move move = new Move(card1, card2);
        //System.out.println(printMove(move)); // Esto imprimirá el movimiento

		return move;
	}

    @Override
    public void update(Move opponentMove) {
        // Aquí debes implementar cómo el jugador se actualiza después de cada movimiento del oponente.
    }

    @Override
    public int getScore() {
        return this.score;
    }

	public void updateScore() {
		this.score = 0;
		for (Card card : hand) {
			this.score += card.getBaseValue();
		}
	}

	@Override
	public void verifyGame(ImmutableList<Integer> scores) throws Exception {
		// Implementa la lógica de verificación aquí...
		for (int score : scores) {
			if (score < 0) {
				throw new Exception("Invalid score.");
			}
		}
	}

    public String printMove(Move move) {
        return "Zug Player " + getName() + ": taken = " + move.getTaken().getName() + ", deposited = " + move.getDeposited().getName();
    }

    public void addCard(Card card, int index) {
		if(card == null) {
			System.out.println("ERROR");
			System.exit(0);
		}
        this.hand.add(card);
		Karte karte = (Karte) card;
		karte.setIndex(index);
    }
    
    public void receiveNewCard(Card newCard, int index) {
        this.newCard = newCard;
		this.addCard(newCard, this.getHand().size());
        this.readyToDiscard = true;
		this.sketchCards();
    }
   
    public void setNewCard(Karte card) {
        this.newCard = (Card) card;
    }

    public void removeCard(Card card) {
        this.hand.remove(card);
    }

	public boolean isBot() {
		return false;
	}

	public List<Card> getHand() {
        return hand;
    }

	public String getHandAsString() {
		StringBuilder sb = new StringBuilder();
        System.out.println("Deine Karten: ");
        List<Card> hand = getHand();
        for (int i = 0; i < hand.size(); i++) {
			sb.append(i+1).append(". ").append(hand.get(i).getName()).append("\n");
        }
		return sb.toString();
	}

    public void setScore(int score) {
        this.score = score;
    }

  	public void setId(int id) {
        this.id = id;
    }
	
    public void chosedCard(int index) {
        this.chosedIndex = index;
    }

	public Card getCardAt(int index) {
		if (index >= 0 && index < hand.size()) {
			return hand.get(index);
		} else {
			throw new IndexOutOfBoundsException("El índice proporcionado no es válido.");
		}
	}
	
	public void sketchCards() {
		GUI gui = GUI.getInstance();
		gui.sketchCards(this.getHand(), 150, 7);
	}

    public boolean getReadyToDiscard() {
        return this.readyToDiscard;
    }

    public void unsetReadyToDiscard() {
		int i = 0;
        for (Card card : hand) {
			Karte karte = (Karte) card;
			karte.setIndex(i);
			i++;
		}
        this.readyToDiscard = false;
    }
}

