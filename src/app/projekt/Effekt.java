package app.projekt;

import bauernhof.preset.card.Effect;
import bauernhof.preset.card.Card;
import bauernhof.preset.card.EffectType;
import bauernhof.preset.card.CardColor;
import java.util.Set;
import bauernhof.preset.Either;

public class Effekt implements Effect {

    private EffectType type;
    private int effectValue;
    private Set<Either<Card,CardColor>> selector;

    public Effekt(EffectType type, int effectValue, Set<Either<Card,CardColor>> selector) {
        this.type = type;
        this.effectValue = effectValue;
        this.selector = selector;
    }

    @Override
    public EffectType getType() {
        return this.type;
    }

    @Override
    public int getEffectValue() {
        return this.effectValue;
    }

    @Override
    public Set<Either<Card,CardColor>> getSelector() {
        return this.selector;
    }

}

