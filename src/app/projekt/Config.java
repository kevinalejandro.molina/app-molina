package app.projekt;

import java.util.Set;
import java.util.HashSet;

import bauernhof.preset.GameConfiguration;
import bauernhof.preset.card.CardColor;
import bauernhof.preset.card.Card;

public class Config implements GameConfiguration {
    // Campos para almacenar la configuración del juego.
    private String configDescription;
    private int numDepositionAreaSlots;
    private int numCardsPerPlayerHand;
    private Set<CardColor> cardColors;
    private Set<Card> cards;
    private String rawConfiguration;
    
    public Config(String configDescription, int numDepositionAreaSlots, 
                                 int numCardsPerPlayerHand, Set<CardColor> cardColors, 
                                 Set<Card> cards, String rawConfiguration) {
        this.configDescription = configDescription;
        this.numDepositionAreaSlots = numDepositionAreaSlots;
        this.numCardsPerPlayerHand = numCardsPerPlayerHand;
        this.cardColors = cardColors;
        this.cards = cards;
        this.rawConfiguration = rawConfiguration;
    }
    
    @Override
    public String getConfigDescription() {
        return this.configDescription;
    }
    
    @Override
    public int getNumDepositionAreaSlots() {
        return this.numDepositionAreaSlots;
    }
    
    @Override
    public int getNumCardsPerPlayerHand() {
        return this.numCardsPerPlayerHand;
    }
    
    @Override
    public Set<CardColor> getCardColors() {
        return this.cardColors;
    }
    
    @Override
    public Set<Card> getCards() {
        return this.cards;
    }

	public Set<Karte> getKarten() {
		Set<Karte> kartenSet = new HashSet<>();

		for (Card card : cards) {
			if (card instanceof Karte) {
				kartenSet.add((Karte) card);
			}
		}

		return kartenSet;
	}

    @Override
    public Card getCardByName(String cardName) {
        for (Card card : this.cards) {
            if (card.getName().equals(cardName)) {
                return card;
            }
        }
        return null;
    }

    @Override
    public String getRawConfiguration() {
        return this.rawConfiguration;
    }
}

