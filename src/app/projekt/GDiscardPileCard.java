package app.projekt;

import java.awt.Color; 
import sag.*; 
import sag.elements.*; 
import sag.elements.shapes.*;
import sag.events.*;


public class GDiscardPileCard extends GKarte {
	private boolean clicked;
	private Spielbrett sb;

    public GDiscardPileCard(Karte karte) {
        super(karte);
		this.clicked = false;
		this.sb = Spielbrett.getInstance();
		this.setListener();
    }

    private MouseEventListener getDiscardPileMouseEventListener(GKarte gkarte) {
        return new MouseEventListener() {
            @Override
            public void mouseClicked(MouseButtonEvent event, GElement self) {
				self.setStroke(Color.RED);
				GDiscardPileCard.this.clicked = true;
				GDiscardPileCard.this.sb.selectDepoCard(GDiscardPileCard.this);

				//sb.getActualPlayer().receiveNewCard(gkarte.karte,sb.getActualPlayer().getHand().size());
            }
			@Override
            public void mouseWheelMoved(MouseWheelEvent event, GElement self) {
                // Implementación básica
            }

            @Override
            public void mouseMoved(MouseMotionEvent event, GElement self) {
                // Implementación básica
            }

            @Override
            public void mouseExited(MouseMotionEvent event, GElement self) {
				if (GDiscardPileCard.this.clicked) {
					self.setStroke(Color.RED);
				} else {
					self.setStroke(Color.BLACK);
				}
            }

            @Override
            public void mouseEntered(MouseMotionEvent event, GElement self) {
				self.setStroke(Color.GREEN);
            }

            @Override
            public void mouseReleased(MouseButtonEvent event, GElement self) {
                // Implementación básica
            }

            @Override
            public void mousePressed(MouseButtonEvent event, GElement self) {
                // Implementación básica
            }
        };
    }

    public void setListener() {
        this.setMouseEventListener(getDiscardPileMouseEventListener(this));
    }

	public void select() {
        this.clicked = true;
        this.setStroke(Color.RED);
    }

    public void deselect() {
        this.clicked = false;
        this.setStroke(Color.BLACK);
    }

}

