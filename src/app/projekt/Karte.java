package app.projekt;

import bauernhof.preset.card.Card;
import bauernhof.preset.card.CardColor;
import bauernhof.preset.card.Effect;
import java.util.Set;

public class Karte implements Card {
	private String name;
    private int baseValue;
    private CardColor color;
    private String image;
    private Set<Effect> effects;
	private int index;

	public Karte(String name, int baseValue, CardColor color, String image) {
        this.name = name;
        this.baseValue = baseValue;
        this.color = color;
        this.image = image;
    }

    public Karte(String name, int baseValue, CardColor color, String image, Set<Effect> effects) {
        this.name = name;
        this.baseValue = baseValue;
        this.color = color;
        this.image = image;
        this.effects = effects;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getBaseValue() {
        return this.baseValue;
    }

    @Override
    public CardColor getColor() {
        return this.color;
    }

    @Override
    public String getImage() {
        return this.image;
    }

    @Override
    public Set<Effect> getEffects() {
        return this.effects;
    }
		
    public void setEffects(Set<Effect> effects) {
        this.effects = effects;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Card) {
            return getName().equals(((Card)obj).getName());
        } else {
            return false;
        }
    }

	public int getIndex() {
		return this.index;
	}

	public void setIndex(int index) {
		this.index = index;
	}
}

