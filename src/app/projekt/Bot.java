package app.projekt;

import bauernhof.preset.card.Card;
import bauernhof.preset.Move;

import javax.swing.Timer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Bot extends Spieler {
	private Karte newCard;
	private int chosedIndex;
	private Random random;
	private Spielbrett sb;


    public Bot(String name) {
        super(name);
		this.random = new Random();
    }

    @Override
	public boolean isBot() {
		return true;
	}

	public Karte prepareMove(int max, Spielbrett sb) {
		int randomNum = ThreadLocalRandom.current().nextInt(0, max + 1);
		
		if (randomNum == 0) {
			return sb.getDeck().drawCard();
		} else {
			return sb.getDiscardPile().removeCardAt(randomNum-1);
		}
	}

    public Move request() throws Exception {
        if (newCard == null) {
            throw new Exception("No new card provided to bot.");
        }

        Card oldCard = getHand().get(0);

        removeCard(oldCard);
		//addCard(newCard, this.getHand().size());	
        Move move = new Move(newCard, oldCard);
        System.out.println(printMove(move)); // Print Zug

        return move;
    }

	public void playTurn(int dpSize) {
		this.sb = Spielbrett.getInstance();
		int randomAction = random.nextInt(dpSize);
		int randomIndex = random.nextInt(getHand().size() - 1);
		if (randomAction == 0) {
			System.out.println("Bot takes from deck");
			Timer timer = new Timer(1000, new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					Karte karte = sb.getDeck().drawCard();
					Bot.this.receiveNewCard(karte);

					Timer timer2 = new Timer(1000, new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							sb.performHumanMove(randomIndex, karte);
						}
					});
					timer2.setRepeats(false); 
					timer2.start();
				}
			});
			timer.setRepeats(false); 
			timer.start();
		} else {
			System.out.println("Bot takes from discard pile");
			Timer timer = new Timer(1000, new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					Karte newKarte = sb.getDiscardPile().removeCardAt(randomAction-1);
					Bot.this.receiveNewCard(newKarte);

					Timer timer2 = new Timer(1000, new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							sb.performHumanMove(randomIndex, newKarte);
						}
					});
					timer2.setRepeats(false); 
					timer2.start();
				}
			});
			timer.setRepeats(false); 
			timer.start();
		}
	}


    public void receiveNewCard(Karte newCard) {
        this.newCard = newCard;
    }

	public void chosedCard(int index) {
        this.chosedIndex = index;
    }
}
