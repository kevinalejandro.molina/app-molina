package app.projekt.UI;

import java.awt.Color; 
import sag.*; 
import sag.elements.*; 
import sag.elements.shapes.*;
import sag.events.*;

public abstract class UIElement {
    protected GElement element;

    public GElement getGElement() {
        return this.element;
    }
}

