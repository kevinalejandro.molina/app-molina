package app.projekt.UI;

import java.awt.Color; 
import sag.*; 
import sag.elements.*; 
import sag.elements.shapes.*;
import sag.events.*;

public class Counter extends UIElement {

    private boolean isCard;
	private GText element;

    private MouseEventListener getListener(GText element, boolean plus) {
        return new MouseEventListener() {
            @Override
            public void mouseClicked(MouseButtonEvent event, GElement self) {
				int currentValue = Integer.parseInt(element.getText());
				currentValue = plus ? currentValue + 1 : currentValue - 1;
                element.setText(Integer.toString(currentValue));
            }

            @Override
            public void mouseWheelMoved(MouseWheelEvent event, GElement self) {
                // Implementación básica
            }

            @Override
            public void mouseMoved(MouseMotionEvent event, GElement self) {
                // Implementación básica
            }

            @Override
            public void mouseExited(MouseMotionEvent event, GElement self) {
                // Implementación básica
            }

            @Override
            public void mouseEntered(MouseMotionEvent event, GElement self) {
                // Implementación básica
            }

            @Override
            public void mouseReleased(MouseButtonEvent event, GElement self) {
                // Implementación básica
            }

            @Override
            public void mousePressed(MouseButtonEvent event, GElement self) {
                // Implementación básica
            }
        };
    }
	//public Counter(int x, int y, int layer, int number, int shift, boolean isCard, GGroup[] layers, MouseEventListener listener) {
	public Counter(int x, int y, GGroup layer, int number, int shift) {
        GText element = new GText(Integer.toString(number));
        String upXY = new String(s(x+shift-6,y-30) + " " + s(x+shift+22,y-30) + " " + s(x+shift+8,y-50));
        String downXY = new String(s(x+shift-6,y+10) + " " + s(x+shift+22,y+10) + " " + s(x+shift+8,y+30));
        GPolyline up = new GPolyline(upXY);
        GPolyline down = new GPolyline(downXY);
        up.setMouseEventListener(getListener(element, true));
        down.setMouseEventListener(getListener(element, false));

        element.setPosition(x,y);
        layer.addChild(element);
        layer.addChild(up);
        layer.addChild(down);
        
        this.element = element;
    }

	private String s (int x, int y) {
		return Integer.toString(x) + "," + Integer.toString(y);
	}

	public int getValue() {
		return Integer.parseInt(element.getText());
	}
}

