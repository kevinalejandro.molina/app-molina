package app.projekt.UI;

import java.awt.Color; 
import sag.*; 
import sag.elements.*; 
import sag.elements.shapes.*;
import sag.events.*;
import java.util.concurrent.CountDownLatch;

public class Button extends UIElement {
    private CountDownLatch startSignal;

	private MouseEventListener getListener() {
        return new MouseEventListener() {
            @Override
            public void mouseClicked(MouseButtonEvent event, GElement self) {
				startSignal.countDown();
            }

            @Override
            public void mouseWheelMoved(MouseWheelEvent event, GElement self) {
                // Implementación básica
            }

            @Override
            public void mouseMoved(MouseMotionEvent event, GElement self) {
                // Implementación básica
            }

            @Override
            public void mouseExited(MouseMotionEvent event, GElement self) {
                // Implementación básica
            }

            @Override
            public void mouseEntered(MouseMotionEvent event, GElement self) {
                // Implementación básica
            }

            @Override
            public void mouseReleased(MouseButtonEvent event, GElement self) {
                // Implementación básica
            }

            @Override
            public void mousePressed(MouseButtonEvent event, GElement self) {
                // Implementación básica
            }
        };
    }

	//public Button(float x, float y, int layer, String text, GGroup[] layers, MouseEventListener listener) {
	public Button(float x, float y, GGroup layer, String text, CountDownLatch startSignal) {
        GText element = new GText(text);
        GRect rect = new GRect(x,y,200f,100f,true,-50f,-50f);
        rect.setFill(Color.GREEN);

        this.startSignal = startSignal;

        rect.setMouseEventListener(getListener());

        element.setPosition(x-35,y+5);
        layer.addChild(rect);
        layer.addChild(element);
        
        this.element = element;
    }

	public Button(float x, float y, GGroup layer, String text) {
        GText element = new GText(text);
        GRect rect = new GRect(x,y,200f,100f,true,-50f,-50f);
        rect.setFill(Color.GREEN);

        rect.setMouseEventListener(getListener());

        element.setPosition(x-65,y+10);
        layer.addChild(rect);
        layer.addChild(element);
        
        this.element = element;
    }
}

