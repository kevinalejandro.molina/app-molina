package app.projekt.UI;

import java.awt.Color; 
import sag.*; 
import sag.elements.*; 
import sag.elements.shapes.*;
import sag.events.*;
import java.util.concurrent.CountDownLatch;
import app.projekt.Karte;
import app.projekt.Spielbrett;
import bauernhof.preset.card.CardColor;

public class DeckButton extends UIElement {
    private CountDownLatch startSignal;
	private Spielbrett sb;
    /**
	 * @hidden
	 */
	protected static final float scale = 4f * 0.75f;
	/**
	 * Width of a {@link GCard}.
	 */
	public static final float WIDTH = 63 * scale;
	/**
	 * Height of a {@link GCard}.
	 */
	public static final float HEIGHT = 88 * scale;
	/**
	 * Background color of a {@link GCard}.
	 */
	public static final Color bgcolor = CardColor.decodeColor("#dac5a8");


	private MouseEventListener getListener(GText element) {
        return new MouseEventListener() {
            @Override
            public void mouseClicked(MouseButtonEvent event, GElement self) {
				if (!sb.getActualPlayer().getReadyToDiscard()) {
					Karte karte = sb.getDeck().drawCard();
					sb.getActualPlayer().receiveNewCard(karte,sb.getActualPlayer().getHand().size());
					element.setText(Integer.toString(sb.getDeck().size()));
					sb.deselectDepoCard();
				} else {
					System.out.println("SCH: Maximal 1 draw");
				}
            }

            @Override
            public void mouseWheelMoved(MouseWheelEvent event, GElement self) {
                // Implementación básica
            }

            @Override
            public void mouseMoved(MouseMotionEvent event, GElement self) {
                // Implementación básica
            }

            @Override
            public void mouseExited(MouseMotionEvent event, GElement self) {
                // Implementación básica
            }

            @Override
            public void mouseEntered(MouseMotionEvent event, GElement self) {
                // Implementación básica
            }

            @Override
            public void mouseReleased(MouseButtonEvent event, GElement self) {
                // Implementación básica
            }

            @Override
            public void mousePressed(MouseButtonEvent event, GElement self) {
                // Implementación básica
            }
        };
    }

	//public Button(float x, float y, int layer, String text, GGroup[] layers, MouseEventListener listener) {
	public DeckButton(GGroup layer) {
		this.sb = Spielbrett.getInstance();
        GText element = new GText(Integer.toString(sb.getDeck().size()));
        GRect rect = new GRect(0f,0f,WIDTH,HEIGHT,true,10,10);
        rect.setFill(bgcolor);

        this.startSignal = startSignal;

        rect.setMouseEventListener(getListener(element));

        element.setPosition(-15,5);
        layer.addChild(rect);
        layer.addChild(element);
        
        this.element = element;
    }
	
	public void redraw(GGroup layer, float x) {
		this.sb = Spielbrett.getInstance();
        GText element = new GText(Integer.toString(sb.getDeck().size()));
        GRect rect = new GRect(x,0f,WIDTH,HEIGHT,true,10,10);
        rect.setFill(bgcolor);

        this.startSignal = startSignal;

        rect.setMouseEventListener(getListener(element));

        element.setPosition(x-15,5);
        layer.addChild(rect);
        layer.addChild(element);
        
        this.element = element;
	}
}

