package app.projekt.UI;

import java.awt.Color; 
import sag.*; 
import sag.elements.*; 
import sag.elements.shapes.*;
import sag.events.*;
import java.util.concurrent.CountDownLatch;
import app.projekt.Spielbrett;
import bauernhof.preset.card.CardColor;

public class PlayerGUI extends UIElement {
	private Spielbrett sb;
	private GText player;
	private GText score;
    
	public PlayerGUI(float x, float y, GGroup layer) {
		this.sb = Spielbrett.getInstance();
		GText player = new GText(sb.getActualPlayer().getName());
		GText score = new GText(Integer.toString(sb.getActualPlayer().getScore()));
        //GRect rect = new GRect(x,y,200f,100f,true);
        //rect.setFill(Color.GREEN);

        player.setPosition(x,y-45);
        score.setPosition(x,y);
        //layer.addChild(rect);
        layer.addChild(player);
        layer.addChild(score);
        //int currentValue = Integer.parseInt(element.getText());
		//		currentValue = plus ? currentValue + 1 : currentValue - 1;
        //        element.setText(Integer.toString(currentValue));

        this.player = player;
        this.score = score;
    }
	
	public void updateGUI() {
		player.setText(sb.getActualPlayer().getName());
		sb.getActualPlayer().updateScore();
		score.setText(Integer.toString(sb.getActualPlayer().getScore()));
	}
	
}

