package app.projekt;

import bauernhof.preset.card.Card;
import bauernhof.preset.Move;
import java.util.Scanner;
import java.util.List;

public class Human extends Spieler {
	private int chosedIndex;

    public Human(String name) {
        super(name);
    }

	@Override
	public boolean isBot() {
		return false;
	}

    @Override
    public Move request() throws Exception {
		System.out.println(getHandAsString());

        Card oldCard = getHand().get(this.chosedIndex);

		removeCard(oldCard);

        Move move = new Move(newCard, oldCard);
        System.out.println(printMove(move));

		this.sketchCards();
		this.unsetReadyToDiscard();
        return move;
    }

	public void chosedCard(int index) {
        this.chosedIndex = index;
    }
}
