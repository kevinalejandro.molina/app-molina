package app.projekt;

import bauernhof.preset.GameConfiguration;
import bauernhof.preset.GameConfigurationException;
import bauernhof.preset.GameConfigurationParser;
import bauernhof.preset.Either;
import bauernhof.preset.card.CardColor;
import bauernhof.preset.card.Effect;
import bauernhof.preset.card.EffectType;
import bauernhof.preset.card.Card;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.IOException;  
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;

public class ConfigParser implements GameConfigurationParser {

	@Override
	public GameConfiguration parse(String filecontents) throws GameConfigurationException {
		// TODO: implementa tu lógica de parseo aquí

		// Esto es solo un marcador de posición, reemplázalo con el objeto GameConfiguration real una vez que hayas parseado el archivo
		GameConfiguration config = null;

		return config;
	}

    @Override
    public GameConfiguration parse(File file) throws GameConfigurationException, IOException {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(file);

			NodeList configDescriptionNodes = doc.getElementsByTagName("configDescription");
			String configDescription = "";
			if(configDescriptionNodes.getLength() > 0) {
				configDescription = configDescriptionNodes.item(0).getTextContent();
			}

			NodeList rawConfigurationNodes = doc.getElementsByTagName("rawConfiguration");
			String rawConfiguration = "";
			if(rawConfigurationNodes.getLength() > 0) {
				rawConfiguration = rawConfigurationNodes.item(0).getTextContent();
			}

			Element numDepositionAreaSlotsElement = (Element) doc.getElementsByTagName("NumDepositionAreaSlots").item(0);
			int numDepositionAreaSlots = 10;
			if (numDepositionAreaSlotsElement != null) {
				numDepositionAreaSlots = Integer.parseInt(numDepositionAreaSlotsElement.getTextContent());
			}

			Element numCardsPerPlayerHandElement = (Element) doc.getElementsByTagName("NumCardsPerPlayerHand").item(0);
			int numCardsPerPlayerHand = 7;
			if (numCardsPerPlayerHandElement != null) {
				numCardsPerPlayerHand = Integer.parseInt(numCardsPerPlayerHandElement.getTextContent());
			}

			Set<CardColor> cardColors = new HashSet<>();
			NodeList colorNodes = doc.getElementsByTagName("CardColor");
			for (int i = 0; i < colorNodes.getLength(); i++) {
				Element colorElement = (Element) colorNodes.item(i);
				String name = colorElement.getTextContent();
				String colorStr = colorElement.getAttribute("color");
				cardColors.add(new CardColor(name, colorStr));
			}

			// Create cards
			Set<Karte> cards = new HashSet<>();
			NodeList cardNodes = doc.getElementsByTagName("Card");
			for (int i = 0; i < cardNodes.getLength(); i++) {
				Element cardElement = (Element) cardNodes.item(i);

				// Obtain cards attr
				String colorName = cardElement.getAttribute("color");
				String basevalue = cardElement.getAttribute("basevalue");
				String name = cardElement.getAttribute("name");
				int baseValue = Integer.parseInt(cardElement.getAttribute("basevalue"));
				String image = cardElement.getAttribute("image");
				CardColor cardColor = cardColors.stream()
												.filter(c -> c.getName().equals(colorName))
												.findFirst()
												.orElse(null);


				Karte card = new Karte(name, baseValue, cardColor, image);

				// Añade la carta al conjunto de cartas
				cards.add(card);
			}
			
			// Add effects to cards
			for (int i = 0; i < cardNodes.getLength(); i++) {
				Element cardElement = (Element) cardNodes.item(i);
				
				String cardRef = cardElement.getAttribute("name");

				Karte refCard = cards.stream()
					.filter(c -> c.getName().equals(cardRef))
					.findFirst()
					.orElse(null);
				

				// Leer los elementos de Effect dentro de Card
				Set<Effect> effects = new HashSet<>();
				NodeList effectNodes = cardElement.getElementsByTagName("Effect");
				for (int j = 0; j < effectNodes.getLength(); j++) {
					Set<Either<Card,CardColor>> selectors = new HashSet<>();
					Element effectElement = (Element) effectNodes.item(j);
					if (refCard != null) {
						selectors.add(new Either<>(refCard, null));
					} else {
						System.out.println("ERROR: Card not found: " + cardRef);
						System.exit(0);
					}
					// Obtén los atributos de Effect
					String type = effectElement.getAttribute("type");
					EffectType effectType = EffectType.valueOf(type);
					int effectValue = Integer.parseInt(effectElement.getAttribute("effectvalue"));

					// Crea un nuevo efecto y añádelo a tu carta
					effects.add(new Effekt(effectType, effectValue, selectors));
				}

				refCard.setEffects(effects);	

			}
			Set<Card> cardSet = cards.stream().map(card -> (Card) card).collect(Collectors.toSet());

            return new Config(configDescription, numDepositionAreaSlots, numCardsPerPlayerHand, cardColors, cardSet, rawConfiguration);
        } catch (Exception e) {
            throw new GameConfigurationException("Error parsing game configuration", e);
        }
    }
}

