package app.projekt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

public class Deck {
    private ArrayList<Karte> cards;

    public Deck() {
        this.cards = new ArrayList<>();
    }

	public Deck(Set<Karte> cards) {
		this.cards = new ArrayList<Karte>(cards);
	}

    // Añade una carta al mazo
    public void addCard(Karte card) {
        this.cards.add(card);
		this.reassignIndices();
    }

	public ArrayList<Karte> getCards() {
        return this.cards;
    }

    // Remueve una carta del mazo
    public Karte removeCard() {
        if (!this.cards.isEmpty()) {
            return this.cards.remove(this.cards.size() - 1);
        } else {
            return null; // o lanza una excepción, según prefieras
        }
    }

	// Remueve una carta del mazo basado en un índice
	public Karte removeCardAt(int index) {
		if(index >= 0 && index < this.cards.size()) {
			return this.cards.remove(index);
		} else {
			return null; // o lanza una excepción si el índice es inválido
		}
	}

    // Verifica si el mazo está vacío
    public boolean isEmpty() {
        return this.cards.isEmpty();
    }

    public void shuffle() {
        Collections.shuffle(this.cards);
    }

	public void clear() {
        this.cards.clear();
    }
	
	public Karte drawCard() {
        if (!this.cards.isEmpty()) {
            return this.cards.remove(this.cards.size() - 1);
        } else {
            return null; // o lanza una excepción, según prefieras
        }
    }

	public int size() {
		return this.cards.size();
	}

	public void reassignIndices() {
		for (int i = 0; i < this.cards.size(); i++) {
			this.cards.get(i).setIndex(i);
		}
	}

}
